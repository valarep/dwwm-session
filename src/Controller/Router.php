<?php
namespace DWWM\Controller;

class Router
{
    public static function getPath()
    {
        $script_name = $_SERVER['SCRIPT_NAME'];
        $exploded = explode('/', $script_name);
        array_pop($exploded);
        $path = implode('/', $exploded);
        return $path;
    }

    public static function getOrigin()
    {
        return $_SERVER['HTTP_ORIGIN'];
    }
}