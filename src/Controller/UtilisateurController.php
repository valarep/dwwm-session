<?php
namespace DWWM\Controller;

use DWWM\Model\Classes\Utilisateur;
use DWWM\View\View;

class UtilisateurController
{
    public static function loginAction()
    {
        $view = new View("login");
        $view->display();
    }

    public static function submitLoginAction($login, $password)
    {
        $user = new Utilisateur(null, $login, $password);
        SessionManager::connect($user);
        if (SessionManager::isConnected())
        {
            // utilisateur valide
            // redirection vers la page d'accueil
            $origin = Router::getOrigin();
            $path = Router::getPath();
            header("location: {$origin}{$path}");
        }
        else
        {
            // utilisateur invalide
            $message = "Login or password error !";
            $view = new View("login");
            $view->bindParam("message", $message);
            $view->display();
        }
    }

    public static function welcomeAction()
    {
        $user = SessionManager::getUser();
        $view = new View("welcome");
        $view->bindParam("user", $user);
        $view->display();
    }

    public static function disconnectAction()
    {
        SessionManager::disconnect();
        // redirection vers la page d'accueil
        $origin = Router::getOrigin();
        $path = Router::getPath();
        header("location: {$origin}{$path}");
    }
}