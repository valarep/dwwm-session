<?php
namespace DWWM\Controller;

use DWWM\Model\Classes\Utilisateur;

class SessionManager
{
    public static function start()
    {
        session_start();
        if (! isset($_SESSION['connected']))
        {
            $_SESSION['connected'] = false;
        }
        self::check();
    }

    public static function check()
    {
        // si on est connecté
        if (self::isConnected())
        {
            $user = json_decode($_SESSION['user']);

            $login = $user->login;
            $password = $user->password;
    
            $user = Utilisateur::getUtilisateur($login, $password);
            if ($user)
            {
                // on a récupéré un object Utilisateur
                $_SESSION['user'] = json_encode($user);
                $_SESSION['connected'] = true;
            }
            else
            {
                // erreur de login ou de password => déconnexion
                self::disconnect();
            }
        }
        else
        {
            // session is disconnected, nothing to do
        }
    }

    public static function disconnect()
    {
        session_destroy();
        session_unset();
        session_start();
        $_SESSION['connected'] = false;
    }

    public static function connect($user)
    {
        $_SESSION['user'] = json_encode($user);
        $_SESSION['connected'] = true;
        self::check();
    }

    public static function getUser()
    {
        return json_decode($_SESSION['user']);
    }

    public static function isConnected()
    {
        return $_SESSION['connected'];
    }
}