<?php
namespace DWWM\Model\Classes;

use DWWM\Model\Dao\UtilisateurDao;

class Utilisateur
{
    // Propriété(s)
    public $id;
    public $login;
    public $password;

    // Constructeur
    public function __construct($id, $login, $password)
    {
        $this->id = $id;
        $this->login = $login;
        $this->password = $password;
    }

    // Méthodes Statiques

    public static function getUtilisateur($login, $password)
    {
        $dao = new UtilisateurDao();
        return $dao->getUtilisateur($login, $password);
    }
}