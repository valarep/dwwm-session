<?php
namespace DWWM;
use DWWM\Controller\UtilisateurController;
use DWWM\Controller\SessionManager;

require "../vendor/autoload.php";
SessionManager::start();

$action = filter_input(INPUT_GET, "action", FILTER_SANITIZE_STRING);
if (empty($action))
{
    // on n'a pas d'action => action par défaut
    if (SessionManager::isConnected())
    {
        // on est connecté => page welcome
        $action = "Welcome";
    }
    else
    {
        // on n'est pas connecté => page login
        $action = "Login";
    }
}

switch ($action)
{
    case "Login":
        UtilisateurController::loginAction();
        break;
    case "SubmitLogin":
        $login = filter_input(INPUT_POST, "login", FILTER_SANITIZE_STRING);
        $password = filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING);
        $password = md5($password);

        UtilisateurController::submitLoginAction($login, $password);
        break;
    case "Welcome":
        UtilisateurController::welcomeAction();
        break;
    case "Disconnect":
        UtilisateurController::disconnectAction();
        break;
}
